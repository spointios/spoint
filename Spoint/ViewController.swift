//
//  ViewController.swift
//  Spoint
//
//  Created by kalyan on 06/11/17.
//  Copyright © 2017 Personal. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.


    }

    func loginWithPhoneNumber(){

        // [START phone_auth]
        PhoneAuthProvider.provider().verifyPhoneNumber("df") { (verificationID, error) in
            // [START_EXCLUDE silent]
            self.hideSpinner {
                // [END_EXCLUDE]
                if let error = error {

                    return
                }
                // Sign in using the verificationID and the code sent to the user
                // [START_EXCLUDE]
                guard let verificationID = verificationID else { return }
            }
            // [END_EXCLUDE]
        }
        // [END phone_auth]

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

